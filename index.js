// All selectors
const html = document.querySelector("html");
const section = document.querySelector("section");
const palmer = document.querySelector(".palmer");
const mountain = document.querySelector(".mountain");
const cultery = document.querySelector(".cultery");
const friends = document.querySelector(".friends");
const footer_arrow = document.querySelector(".footer_arrow");
const scrolling_navbar = document.getElementById("second_nav_bar");
const circle = document.querySelector(".circle");
const sidenav = document.querySelector(".sidenav");
const input = document.getElementById("menu_btn");

// Scrolling events
document.addEventListener("scroll", (e) => {
  // Make appear drawings
  if (html.scrollTop >= section.clientHeight * 4.1) {
    footer_arrow.style.animation = "footer_arrow_appearance 4s forwards";
  } else if (html.scrollTop >= section.clientHeight * 3.5) {
    friends.style.animation = "friends_appearance 1.5s forwards";
  } else if (html.scrollTop >= section.clientHeight * 2.5) {
    cultery.style.animation = "cultery_appearance 1.5s forwards";
  } else if (html.scrollTop >= section.clientHeight * 1.5) {
    mountain.style.animation = "mountain_appearance 1.5s forwards";
  } else if (html.scrollTop >= section.clientHeight / 2) {
    palmer.style.animation = "palmer_appearance 1.5s forwards";
  }

  // Make appear/disappear the nav bar between 1st/2nd pages transition
  // if (html.scrollTop <= section.clientHeight - (section.clientHeight * 0.1) ) {
  //   scrolling_navbar.style.visibility = "hidden";
  // } else {
  //   scrolling_navbar.style.visibility = "visible";
  // }

  // Make nav bar follow the scroll from the second section and
  if (html.scrollTop >= section.clientHeight) {
    scrolling_navbar.style.top = html.scrollTop + "px";
  } else if (html.scrollTop < section.clientHeight) {
    scrolling_navbar.style.top = section.clientHeight + "px";
  }
});

// Mouse cursor tracker
var $circle = $(".circle");

function moveCircle(e) {
  TweenLite.to($circle, 0.3, {
    css: {
      left: e.pageX,
      top: e.pageY,
    },
  });

  // Change the border color of the mouse cursor tracker
  if (e.pageY >= section.clientHeight) {
    circle.style.border = "2px solid #fca3a3";
  } else if (e.pageX < section.clientHeight) {
    circle.style.border = "2px solid white";
  }
}

$(window).on("mousemove", moveCircle);

// Toggle sidebar menu button
function toggleNavBar() {
  var shown = sidenav.getAttribute("data-isshown");

  if (shown == null || shown == "" || shown == "true") {
    sidenav.setAttribute("data-isshown", "false");
    sidenav.style.boxShadow = "-4px 3px 25px -9px #000000";
    sidenav.style.right = "0%";
    input.checked = true;
  } else {
    sidenav.setAttribute("data-isshown", "true");
    sidenav.style.boxShadow = "none";
    sidenav.style.right = "-45%";
    input.checked = false;
  }
}

// Hide side bar menu when clicking outside menu (mobile version)
document.addEventListener("click", (e) => {
  if (sidenav.style.right == "0%") {
    if (e.clientX < window.innerWidth * 0.55) {
      sidenav.style.boxShadow = "none";
      sidenav.style.right = "-45%";
      sidenav.setAttribute("data-isshown", "true");
      input.checked = false;
    }
  }
});

// Handle carousel
var slideIndex = 1;
var landscapes_slides = document.querySelectorAll("#landscapes_page .mySlides");
var foods_slides = document.querySelectorAll("#foods_page .mySlides");
var meetings_slides = document.querySelectorAll("#meetings_page .mySlides");

showSlides(slideIndex, landscapes_slides);
showSlides(slideIndex, foods_slides);
showSlides(slideIndex, meetings_slides);

function landscapesNextSlides(n) {
  showSlides((slideIndex += n), landscapes_slides);
}

function foodsNextSlides(n) {
  showSlides((slideIndex += n), foods_slides);
}

function meetingsNextSlides(n) {
  showSlides((slideIndex += n), meetings_slides);
}

function showSlides(n, slides) {
  var i;
  if (n > slides.length) {
    slideIndex = 1;
  }
  if (n < 1) {
    slideIndex = slides.length;
  }
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  slides[slideIndex - 1].style.display = "block";
}
