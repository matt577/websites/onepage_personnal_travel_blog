## Make icon fit to its parent div

HTML:

    <a href="#home_page"
    ><img src="assets/icons/accueil_blanc.png" alt=""
    /></a>

CSS:

    a {
      width: 40px;
      height: 40px;
    }

    img {
      min-width: 100%;
      max-width: 100%;
    }

## Make navigation between page smooth on one-page website

    html {
        scroll-behavior: smooth;
    }

## Make a video background responsive

https://redstapler.co/responsive-css-video-background/

The 16/9 ration should be keep. In order to do that media querries should be used :

    @media (min-aspect-ratio: 16/9) {
      video {
        width: 100%;
        height: auto;
      }
    }

    @media (max-aspect-ratio: 16/9) {
      video {
        width: auto;
        height: 100%;
      }
    }

## Which units to use

https://gist.github.com/basham/2175a16ab7c60ce8e001

## Make a div get the all available height

The idea is to use `flex` properties to spread correctly the space. The following snippet of code demonstrate that :

    <head>
      <title>Title of the document</title>
      <style>
        html,
        body {
          height: 100%;
          margin: 0;
        }
        .box {
          display: flex;
          flex-flow: column;
          height: 100%;
        }
        .box .row {
          border: 1px dotted #0313fc;
        }
        .box .row.header {
          flex: 0 1 auto;
        }
        .box .row.content {
          flex: 1 1 auto;
        }
        .box .row.footer {
          flex: 0 1 40px;
        }
      </style>
    </head>
    <body>
      <div class="box">
        <div class="row header">
          <p>header (sized to content)</p>
        </div>
        <div class="row content">
          <p>content (fills remaining space)</p>
        </div>
        <div class="row footer">
          <p>footer (fixed height)</p>
        </div>
      </div>
    </body>

Here the `flex` property correspond to `flex-grow`, `flex-shrink` et `flex-basis`.

## Make image responsive

    img {
      display: block;
      max-width: 100%;
    }
